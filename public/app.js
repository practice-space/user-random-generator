const app = Vue.createApp({
    data() {
        return {
            isDisabled: false,
            clickCount: 0,
            firstName: 'Majnun',
            lastName: 'Laila',
            email: 'laila@mail.com',
            gender: 'female',
            picture: 'https://randomuser.me/api/portraits/women/18.jpg',
        }
    },
    methods: {
        async getUsers () {
            if (this.isDisabled) return; // Button is disabled, halt block execution
      
            this.clickCount++;
            this.isDisabled = true;
            
            setTimeout(() => {
                this.isDisabled = false;
            }, 2000);

            const res = await fetch('https://randomuser.me/api');
            const { results } = await res.json();

            // console.log(results)
            this.firstName = results[0].name.first
            this.lastName = results[0].name.last
            this.email = results[0].email
            this.gender = results[0].gender
            this.picture = results[0].picture.large
        },
        // countDownTimer() {
            
        // }
        
    }
})

// document.getElementById("votebutton").disabled = true;
// setTimeout(function(){document.getElementById("votebutton").disabled = false;},5000);

app.mount('#app')